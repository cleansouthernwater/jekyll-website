---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: single
classes: wide
toc: true
toc_label: In this toolkit
toc_sticky: true
toc_icon: poo

sidebar:
  - title: "Get involved!"
    image: /front-cover.jpg
    image_alt: "Strike now"
    nav: docs
---
# What is a payment strike?

A payment strike means refusing to pay your bill to Southern Water until demands are met.

# On what grounds is the strike based?

Southern Water typically charge wastewater at 92.5% of the supplied water. That means that if you are supplied 50m3 of clean water, you will pay wastewater charges for 46.25m3 (50m3 x 92.5%). When Southern Water release waste water untreated, they are not fulfiling their service obligations AND are causing massive environmental harm. And you continue to pay for a full service.

Southren Water may argue that during heavy rainfall, they are allowed to release untreated sewage. Legal or illegal, we do no support the dumping of untreated waste water into the environment!

Fines, petitions, and other forms of governmental regulation have done little to change Southern Waters behaviour over the last 15 years. In fact, the government relaxed regulations as soon as suppy chains for the required chemicals came under pressure in 2020.

If you too feel trapped into this toxic system, then join our payment strike. Individuals banding together to say tell Southern Water:
> #notwithmymoney

## Template letter

Here is a sample letter that you can copy or use for inspiration: [https://docs.google.com/document/d/1nBoGEuPJCB9lN-YzncqRU53Sa-_MuBSsNvYmJk7y4SI/edit?usp=sharing](https://docs.google.com/document/d/1nBoGEuPJCB9lN-YzncqRU53Sa-_MuBSsNvYmJk7y4SI/edit?usp=sharing)

# Important Information

If you choose to take part, there are considerations to be made as there are real-life consequences. Most notably:
* Your credit score may be affected (for all named persons on the bill)
* A County Court Judgement may be taken out against you

# I want to take part!

Thank you! It’s pretty simple:

1. Write/email to Southern Water informing them of your intentions. We’ve prepared a [template](https://docs.google.com/document/d/1nBoGEuPJCB9lN-YzncqRU53Sa-_MuBSsNvYmJk7y4SI/edit?usp=sharing).
2. Change at least the parts in yellow with your personal information. But feel free to express yourself and change whatever else you need.
3. And then stop making payments to Southern Water and keep the money aside. It’s important to have the money available should you need to settle your account.

# What might happen if I don’t pay my water bill?

1. You will possibly receive a warning letter within 14 days of non-payment
2. IF they report default payments to a credit agency, the missed payment will be placed on your file
3. Each month a payment is missed, this will be added to your file
4. These missed payments will stay on your record for three years
5. After six missed payments, a default will be reported to the Credit Reference Agency
6. Defaults will stay on your credit record for six years
7. At this point you will likely receive a County Court Judgement

If you keep your outstanding balance under £50 (fifty pounds) defaults and County Court Judgements are less likely to be issued

If you have any questions, please contact us at cleansouthernwater@protonmail.com
