---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: single
classes: wide

sidebar:
  - title: "Get involved!"
    image: /front-cover.jpg
    image_alt: "Strike now"
    nav: docs
---

Southern Water now may well hold the #1 spot as the most notorious corporate villain in the South East. It’s antics of polluting the waterways and coastlines of Kent have spurred a mass of campaigns against them.

Surfers Against Sewage, the Clean Water Action Group in Hastings, the Chichester Harbour Trust, Save our Harbour Villages, Hayling Sewage Watch. You can also easily add anyone or group who enjoys water activities: open water swimmers, sailors, paddleboarders. And anyone else who doesn’t appreciate sewage in our waterways and coastline.

Sadly fining them doesn’t seem to work. They don’t seem willing to change what they are doing and their track record of major decision making is poor. Southern Water owners and management must account for their actions.

Southern Waters notoriety is well deserved:
1. Southern Water are one of the worst polluters by far
2. Southern Water recently admitted to illegally dumping sewage thousands of times over a five-year period. For this, they received record fines but it won’t be the management or shareholders that foot the bill…
3. Southern Water have deliberately misled the public about their performance
4. Southern Water don’t pay tax in the UK
5. Southern Water have sold a majority share of the company to Macquarie, an Australian finance firm which left Thames Water saddled with debt
6. Despite this rap sheet their still CEO enjoys massive bonuses
7. And the government seems more willing to relax regulations than harden them

# References

1. https://www.bbc.com/news/uk-england-sussex-57804727
2. https://www.kentonline.co.uk/news/national/southern-water-fined-record-90m-after-admitting-criminal-sewage-dumping-35299
3. https://www.islandecho.co.uk/islanders-to-receive-61-rebate-as-southern-water-hit-with-126million-penalty
4. https://www.techregister.co.uk/three-uk-water-companies-hang-on-to-tax-haven-subsidiaries
5. https://www.theguardian.com/business/2021/aug/09/macquarie-wades-back-into-uk-with-majority-stake-in-southern-water
6. https://www.theargus.co.uk/news/19511032.why-southern-waters-chief-executive-got-bonus
7. https://www.theguardian.com/politics/2021/sep/07/government-ease-sewage-discharge-rules-amid-chemical-shortage
