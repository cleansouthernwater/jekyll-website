---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: single
classes: wide

sidebar:
  - title: "Get involved!"
    image: /front-cover.jpg
    image_alt: "Strike now"
    nav: docs
---
Southern Water are dumping untreated sewage into our rivers and seas on an almost daily basis. And it's not the first time.

This is not the service we are paying for. And because Southern Water have the monpoly, we cannot even change providers.

The government is also relaxing regulations regarding when water companies can dump untreated wastewater.

It's time we stood up for our environment and stop pay for a service we aren't getting.

# Join the payment strike!

We are no longer willing to continue paying for the rampant destruction of our environment.

Stop paying for a service you aren't getting.

Read the [payment strike toolkit](/toolkit) for everything you need to know!

# Have you already stopped paying?

We are interested in hearing about your experience. Espesically if Southern Water have been in contact with you!
